package com.PerfumeEcom.Repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.PerfumeEcom.Model.Cart;
import com.PerfumeEcom.Model.User;

public interface CartRepository extends JpaRepository<Cart,Integer> {
	 Optional<Cart> findByUser(User user);

}
