package com.PerfumeEcom.Repository;


import org.springframework.data.jpa.repository.JpaRepository;

import com.PerfumeEcom.Model.Category;

public interface CategoryRepository extends JpaRepository<Category,Integer>{

}
