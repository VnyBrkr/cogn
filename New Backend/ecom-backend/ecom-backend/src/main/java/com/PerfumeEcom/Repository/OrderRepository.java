package com.PerfumeEcom.Repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.PerfumeEcom.Model.Order;
import com.PerfumeEcom.Model.User;

public interface OrderRepository extends JpaRepository<Order,Integer>{
	
  Optional<List<Order>>findByUser(User findByEmail);
  
  
  
  

}
