package com.PerfumeEcom.Repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.PerfumeEcom.Model.Category;
import com.PerfumeEcom.Model.Product;
import com.PerfumeEcom.payload.ProductDto;

public interface ProductRepository extends JpaRepository<Product,Integer> {
	
	Page<Product> findByCategory(Category category,Pageable pageable);
	List<Product> findByProductNameContaining(String pName);
	
	

}
