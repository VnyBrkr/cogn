package com.PerfumeEcom.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.PerfumeEcom.Model.Role;

@Repository
public interface RoleRepository extends JpaRepository<Role,Integer> {
	 

}
