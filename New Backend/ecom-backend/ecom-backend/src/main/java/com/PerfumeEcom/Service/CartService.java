package com.PerfumeEcom.Service;

import com.PerfumeEcom.Model.Cart;
import com.PerfumeEcom.payload.CartDto;
import com.PerfumeEcom.payload.CartItemDto;
import com.PerfumeEcom.payload.ItemRequest;

public interface CartService {
	
	CartDto addItem(ItemRequest item,String UserName);
	CartDto getCart(String UserName);
	CartDto  removeCartItem(String UserName,int productId);

}
