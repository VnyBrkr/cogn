package com.PerfumeEcom.Service;

import java.util.List;

import com.PerfumeEcom.Model.Order;
import com.PerfumeEcom.payload.ItemRequest;
import com.PerfumeEcom.payload.OrderDto;
import com.PerfumeEcom.payload.OrderRequest;

public interface OrderService {
 public OrderDto create(OrderRequest request,String username);
 //Get OrderBy User
 public List<OrderDto> getAllOrder(String p);
 //Get All Order
 public List<OrderDto>listAllOrder();
 public OrderDto getOrder(int OrderId);
 public void deleteOrder(int orderId);
 public OrderDto updateOrder(int orderId,OrderDto orderDto);

 

}
