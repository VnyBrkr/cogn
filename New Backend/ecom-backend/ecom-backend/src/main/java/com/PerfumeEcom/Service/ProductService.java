package com.PerfumeEcom.Service;

import java.util.List;

import com.PerfumeEcom.Model.Product;
import com.PerfumeEcom.payload.ApiResponse;
import com.PerfumeEcom.payload.ProductDto;
import com.PerfumeEcom.payload.ProductResponse;

public interface ProductService {
	
	public ProductDto createProduct(ProductDto productDto,int categoryId);
	public ProductResponse getAllProducts(int PageNumber,int pageSize,String sortBy,String sortDir);
	public ProductDto getProduct(int productId);
	public int deleteProduct(int pid);
	public ProductDto updateProduct(int productId,ProductDto newproduct);
	  ProductResponse getProductByCatgory(int categoryId,int pageSize,int pageNumber);
	public List<ProductDto> findProduct(String pname);


}
